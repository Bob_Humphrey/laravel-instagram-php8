<?php

namespace App\Console\Commands;

use App\Actions\GetLeadsAction;
use Illuminate\Console\Command;

class GetLeads extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:get_leads';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Generate leads from a followers list';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {
    GetLeadsAction::execute();
  }
}
