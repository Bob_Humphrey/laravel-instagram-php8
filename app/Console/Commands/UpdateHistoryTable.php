<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Actions\UpdateHistoryTableAction;

class UpdateHistoryTable extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:update_history';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Save daily total of followers and following';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {
    UpdateHistoryTableAction::execute();
  }
}
