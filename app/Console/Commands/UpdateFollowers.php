<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Actions\UpdateFollowersAction;

class UpdateFollowers extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:update_followers';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Update the followers.';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {
    UpdateFollowersAction::execute();
  }
}
