<?php

namespace App\Http\Livewire;

use App\InstagramUser;
use Livewire\Component;

class LeadSources extends Component
{
  public $instagramUsers;

  public function toggleLeadSource($id)
  {
    $instagramUser = InstagramUser::find($id);
    $instagramUser->lead_source = !$instagramUser->lead_source;
    $instagramUser->save();
    $this->getInstagramUsers();
  }

  public function toggleLeadSourceUsed($id)
  {
    $instagramUser = InstagramUser::find($id);
    $instagramUser->lead_source_used = !$instagramUser->lead_source_used;
    $instagramUser->save();
    $this->getInstagramUsers();
  }

  public function mount()
  {
    $this->getInstagramUsers();
  }

  public function render()
  {
    return view('livewire.lead-sources');
  }

  protected function getInstagramUsers()
  {
    $this->instagramUsers = InstagramUser
      ::select('id', 'name', 'user_name', 'follower', 'following', 'ever_follower', 'ever_following', 'lead_source', 'lead_source_used', 'created_at')
      ->where('lead_source', true)
      ->orderBy('id', 'DESC')
      ->get();
  }
}
