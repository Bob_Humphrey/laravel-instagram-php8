<?php

namespace App\Http\Livewire;

use App\InstagramUser;
use Illuminate\View\View;
use Livewire\Component;

class LeadList extends Component
{
  public $instagramUsers;

  public function toggleLeadSource($id)
  {
    $lead = InstagramUser::whereId($id)->first();
    $lead->lead_source = !$lead->lead_source;
    $lead->save();
    $this->getLeads();
  }

  public function updateLeadRating($id, $rating)
  {
    $lead = InstagramUser::whereId($id)->first();
    $lead->lead_rating = $rating;
    $lead->lead_reject = false;
    $lead->save();
    $this->getLeads();
  }

  public function toggleLeadReject($id)
  {
    $lead = InstagramUser::whereId($id)->first();
    $lead->lead_reject = !$lead->lead_reject;
    $lead->lead_rating = 0;
    $lead->save();
    $this->getLeads();
  }

  public function toggleStoppingPoint($id)
  {
    $lead = InstagramUser::whereId($id)->first();
    $lead->current_lead_stopping_point = !$lead->current_lead_stopping_point;
    $lead->save();
    $this->getLeads();
  }

  public function mount(): void
  {
    $this->getLeads();
  }

  public function render(): View
  {
    return view('livewire.lead-list');
  }

  private function getLeads(): void
  {
    $this->instagramUsers = InstagramUser::where('current_lead', 1)->orderBy('id')->get();
  }
}
