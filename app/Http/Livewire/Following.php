<?php

namespace App\Http\Livewire;

use App\InstagramUser;
use Livewire\Component;
use Illuminate\View\View;
use Illuminate\Support\Facades\Log;

class Following extends Component
{
  public $state = [
    'followings' => [],
    'unfollows' => [],
    'following_count' => 0,
    'follow_back_count' => 0,
    'unfollow_count' => 0,
    'sort_field' => 'started_following_date',
    'sort_direction' => 'DESC',
    'sort_next' => [
      'name' => 'ASC',
      'user_name' => 'ASC',
      'always_following' => 'DESC',
      'stop_following' => 'DESC',
      'follower' => 'ASC',
      'lead_rating' => 'ASC',
      'started_following_date' => 'DESC'
    ]
  ];

  public function handleSort($field): void
  {
    if ($field === $this->state['sort_field']) {
      // Change sort direction
      $previousDirection = $this->state['sort_next'][$field];
      $newDirection = $previousDirection === 'ASC' ? 'DESC' : 'ASC';
      $this->state['sort_next'][$field] = $newDirection;
    }
    $this->state['sort_field'] = $field;
    $this->state['sort_direction'] = $this->state['sort_next'][$field];
    $this->getFollowings();
  }

  public function toggleNeverStop($id): void
  {
    $following = InstagramUser::whereId($id)->first();
    $following->always_following = !$following->always_following;
    if ($following->always_following) {
      $following->stop_following = false;
    }
    $following->save();
    $this->getFollowings();
  }

  public function mount(): void
  {
    $this->getFollowings();
  }

  public function render(): View
  {
    return view('livewire.following');
  }

  private function getFollowings(): void
  {
    if ($this->state['sort_field'] === 'started_following_date') {
      $this->state['followings']
        = InstagramUser::where('following', 1)
        ->orderBy($this->state['sort_field'], $this->state['sort_direction'])->orderBy('lead_rating', 'ASC')
        ->get();
    } else {
      $this->state['followings']
        = InstagramUser::where('following', 1)
        ->orderBy($this->state['sort_field'], $this->state['sort_direction'])->orderBy('started_following_date', 'ASC')
        ->get();
    }

    $this->state['following_count'] = $this->state['followings']->count();

    $this->state['follow_back_count']
      = InstagramUser::where('following', 1)
      ->where('follower', 1)
      ->count();

    $this->state['unfollow_count']
      = InstagramUser::where('stop_following', 1)
      ->count();
  }
}
