<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class Login extends Component
{

  public $email = '';
  public $password = '';
  public $error = '';

  public function submit(Request $request)
  {
    $this->validate([
      'email'    => 'required|email',
      'password' => 'required',
    ]);

    if (Auth::attempt([
      'email' => $this->email,
      'password' => $this->password
    ])) {
      return redirect(route('home'));
    } else {
      $this->error = 'Invalid email and/or password';
    }
  }

  public function render(): View
  {
    return view('livewire.login');
  }
}
