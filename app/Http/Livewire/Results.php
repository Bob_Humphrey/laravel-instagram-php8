<?php

namespace App\Http\Livewire;

use App\InstagramUser;
use Livewire\Component;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class Results extends Component
{
  public $results = [];

  public function mount()
  {
    $date = Carbon::today();
    $lastDate = Carbon::today()->subDays(11);
    do {
      $result = [];
      $result['date'] = $date->format('l F j');
      $instagramUsers = InstagramUser::where('started_following_date', $date->format('Y-m-d'))->get();
      $result['following'] = $instagramUsers->count();
      $followers = $instagramUsers->filter(function ($instagramUser) {
        return $instagramUser->follower;
      });
      $result['followers'] = $followers->count();
      $result['percent'] =
        $instagramUsers->count() === 0 ?
        0 :
        number_format(($followers->count() / $instagramUsers->count()) * 100);
      $followingFromLeads = $instagramUsers->filter(function ($instagramUser) {
        return $instagramUser->lead_rating > 0;
      });
      $result['followingFromLeads'] = $followingFromLeads->count();
      $followersFromLeads = $followingFromLeads->filter(function ($instagramUser) {
        return $instagramUser->follower;
      });
      $result['followersFromLeads'] = $followersFromLeads->count();
      $result['leadsPercent'] =
        $followingFromLeads->count() === 0 ?
        0 :
        number_format(($followersFromLeads->count() / $followingFromLeads->count()) * 100);
      $this->results[] = $result;
      $date->subDay(1);
    } while ($date->greaterThan($lastDate));
  }

  public function render()
  {
    return view('livewire.results');
  }
}
