<?php

namespace App\Http\Controllers;

use App\History;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class HistoryController extends Controller
{
  private $previousFollowers;
  private $previousFollowing;
  private $previousLost;
  private $previousStoppedFollowing;

  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request): View
  {
    $this->previousFollowers = 0;
    $this->previousFollowing = 0;
    $this->previousLost = 0;
    $this->previousStoppedFollowing = 0;

    $collection = History::limit(500)->orderBy('created_at', 'ASC')->get();
    $records = $collection->map(function ($value, $key) {
      $value->followers_change = $value->followers - $this->previousFollowers;
      $this->previousFollowers = $value->followers;
      $value->following_change = $value->following - $this->previousFollowing;
      $this->previousFollowing = $value->following;
      $value->stopped_being_followers_change = $value->stopped_being_followers - $this->previousLost;
      $this->previousLost = $value->stopped_being_followers;
      $value->stopped_following_change = $value->stopped_following - $this->previousStoppedFollowing;
      $this->previousStoppedFollowing = $value->stopped_following;
      return $value;
    })->reverse();
    return view('history', ['records' => $records]);
  }
}
