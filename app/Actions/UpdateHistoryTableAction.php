<?php

namespace App\Actions;

use App\History;
use App\Follower;
use App\Following;
use App\InstagramUser;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class UpdateHistoryTableAction
{
  public static function execute(): void
  {
    $following = InstagramUser::where('following', 1)->get()->count();
    $stopped_following = InstagramUser::where('following', 0)
      ->where('ever_following', 1)
      ->get()
      ->count();

    $followers = InstagramUser::where('follower', 1)->get()->count();
    $stopped_being_followers = InstagramUser::where('follower', 0)
      ->where('ever_follower', 1)
      ->get()
      ->count();

    $history = new History();
    $history->following = $following;
    $history->stopped_following = $stopped_following;
    $history->followers = $followers;
    $history->stopped_being_followers = $stopped_being_followers;
    $history->save();
  }
}
