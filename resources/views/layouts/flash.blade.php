<x-alert type="success" class="bg-green-700 text-green-100 p-4" />
<x-alert type="warning" class="bg-yellow-700 text-yellow-100 p-4" />
<x-alert type="danger" class="bg-red-700 text-red-100 p-4" />
