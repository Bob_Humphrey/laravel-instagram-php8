@extends('layouts.app')

@section('content')

  @php
  use Carbon\Carbon;
  @endphp

  <div class="w-full justify-center py-6">
    <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4">
      History
    </h2>

    <table class="w-full">
      @foreach ($records as $record)

        @php
        $ratio = $record->following === 0 ? 0 : number_format($record->followers / $record->following, 2) ;
        @endphp

        @if ($loop->index % 14 === 0)
          <tr class="grid grid-cols-12 bg-blue-50 text-sm font-nunito_bold border-b border-gray-300">
            <th class="col-span-3 text-left py-3 pl-3">Date</th>
            <th class="col-span-1 text-right py-3">Ratio</th>
            <th class="col-span-2 text-center py-3">Followers</th>
            <th class="col-span-2 text-center py-3">Following</th>
            <th class="col-span-2 text-center py-3">Lost Followers</th>
            <th class="col-span-2 text-center py-3 pr-3">Stopped Following</th>
          </tr>
        @endif

        <tr
          class="grid grid-cols-12 text-sm font-nunito_light border-b border-gray-300 {{ $loop->odd ? '' : 'bg-gray-50' }}">
          <td class="col-span-3 text-left py-3 pl-3">
            {{ Carbon::createFromDate(substr($record->created_at, 0, 10))->format('l, F j, Y') }}
          </td>
          <td class="col-span-1 text-right py-3">
            {{ $ratio }}
          </td>
          <td class="col-span-1 font-nunito_bold text-right py-3">
            {{ number_format($record->followers) }}
          </td>
          <td class="col-span-1 text-right py-3">
            {{ number_format($record->followers_change) }}
          </td>
          <td class="col-span-1 font-nunito_bold text-right py-3">
            {{ number_format($record->following) }}
          </td>
          <td class="col-span-1 text-right py-3">
            {{ number_format($record->following_change) }}
          </td>
          <td class="col-span-1 font-nunito_bold text-right py-3">
            {{ number_format($record->stopped_being_followers) }}
          </td>
          <td class="col-span-1 text-right py-3">
            {{ number_format($record->stopped_being_followers_change) }}
          </td>
          <td class="col-span-1 font-nunito_bold text-right py-3 pr-3">
            {{ number_format($record->stopped_following) }}
          </td>
          <td class="col-span-1 text-right py-3 pr-3">
            {{ number_format($record->stopped_following_change) }}
          </td>
        </tr>
      @endforeach
    </table>
  </div>

@endsection
