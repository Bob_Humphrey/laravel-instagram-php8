@php
use Carbon\Carbon;
@endphp

<div class="w-full justify-center py-6">
  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4">
    Following
  </h2>
  <div class="grid grid-cols-12 mb-4">
    <div class="col-span-2 text-xl font-nunito_light">
      Following
    </div>
    <div class="col-span-2 text-xl font-nunito_bold">
      {{ $state['following_count'] }}
    </div>
    <div class="col-span-2 text-xl font-nunito_light">
      Follow Back
    </div>
    <div class="col-span-2 text-xl font-nunito_bold">
      {{ $state['follow_back_count'] }}
    </div>
    <div class="col-span-3 text-xl font-nunito_light">
      Ready to Unfollow
    </div>
    <div class="col-span-1 text-xl font-nunito_bold">
      {{ $state['unfollow_count'] }}
    </div>
  </div>

  <table class="w-full">
    @foreach ($state['followings'] as $following)
      @if ($loop->index % 14 === 0)
        <tr class="grid grid-cols-12 bg-blue-50 text-sm font-nunito_bold border-b border-gray-300">
          <th class="col-span-3 flex justify-start py-3 pl-3 cursor-pointer" wire:click="handleSort('name')">
            Name
            <div
              class="{{ $state['sort_field'] === 'name' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'name' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
          <th class="col-span-3 flex justify-start py-3 cursor-pointer" wire:click="handleSort('user_name')">
            User Name
            <div
              class="{{ $state['sort_field'] === 'user_name' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'user_name' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
          <th class="col-span-1 flex justify-center py-3 cursor-pointer" wire:click="handleSort('always_following')">
            Keep
            <div
              class="{{ $state['sort_field'] === 'always_following' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'always_following' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
          <th class="col-span-1 flex justify-center py-3 cursor-pointer" wire:click="handleSort('stop_following')">
            Stop
            <div
              class="{{ $state['sort_field'] === 'stop_following' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'stop_following' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
          <th class="col-span-1 flex justify-center py-3 cursor-pointer" wire:click="handleSort('follower')">
            F Back
            <div
              class="{{ $state['sort_field'] === 'follower' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'follower' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
          <th class="col-span-1 flex justify-center py-3 cursor-pointer" wire:click="handleSort('lead_rating')">
            Rating
            <div
              class="{{ $state['sort_field'] === 'lead_rating' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'lead_rating' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
          <th class="col-span-2 flex justify-end py-3 pr-3 cursor-pointer"
            wire:click="handleSort('started_following_date')">
            Start Date
            <div
              class="{{ $state['sort_field'] === 'started_following_date' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'started_following_date' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
        </tr>
      @endif

      <tr
        class="grid grid-cols-12 text-sm font-nunito_light border-b border-gray-300 {{ $loop->odd ? '' : 'bg-gray-50' }}">
        <td class="col-span-3 text-left py-3 pl-3">
          {{ $following->name }}
        </td>
        <td class="col-span-3 text-left py-3">
          <a href="https://www.instagram.com/{{ $following->user_name }}/" class="text-blue-500 cursor-pointer"
            target="_blank" rel="noopener noreferrer">
            {{ $following->user_name }}
          </a>
        </td>
        <td class="col-span-1 flex items-center w-full py-3">
          <div
            class="w-5 h-5 rounded-full mx-auto {{ $following->always_following ? 'bg-green-500' : 'bg-gray-300' }} cursor-pointer"
            wire:click="toggleNeverStop({{ $following->id }})">
          </div>
        </td>
        <td class=" col-span-1 flex items-center w-full py-3">
          <div class="w-5 h-5 rounded-full mx-auto {{ $following->stop_following ? 'bg-red-500' : '' }}">
          </div>
        </td>
        <td class=" col-span-1 flex items-center w-full py-3">
          <div class="w-5 h-5 rounded-full mx-auto {{ $following->follower ? 'bg-green-500' : 'bg-red-500' }}">
          </div>
        </td>
        @php
        $ratings = ['0' => '', '1' => 'A', '2' => 'B', '3' => 'C'];
        $rating = $ratings[strval($following->lead_rating)];
        @endphp
        <td class=" col-span-1 text-center w-full py-3">
          {{ $rating }}
        </td>
        <td class="col-span-2 text-right py-3 pr-3">
          {{ Carbon::createFromDate($following->started_following_date)->format('m-d-y') }}
        </td>
      </tr>
    @endforeach
  </table>
</div>
