<div class="my-10">
  <x-alert type="success" class="w-3/5 bg-green-700 text-white p-4 mb-4 mx-auto" />

  <section class="rounded py-8 px-16 w-3/5 bg-gray-200 mx-auto">

    <h2 class="text-3xl text-center text-blue-800 font-nunito_bold mb-8">
      Instagram User
    </h2>

    {{-- USER NAME STATUS --}}

    <div class="text-center text-blue-500 mb-4">
      {{ $userNameStatus }}
    </div>

    {{-- USER NAME --}}

    <div class="grid grid-cols-12">
      <div class="col-span-3 flex flex-col self-center text-right pr-4">
        <x-label for="user_name" />
      </div>
      <div class="col-span-7">
        <x-input name="user_name" wire:model="user_name"
          class="p-2 rounded border border-gray-200 w-full appearance-none" disabled />
      </div>
    </div>

    <div class="grid grid-cols-12 mb-8">
      <div class="col-span-3">
      </div>
      <div class="col-span-9">
        <x-error field="user_name" class="text-red-500 text-sm">
          <ul>
            @foreach ($component->messages($errors) as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </x-error>
      </div>
    </div>

    {{-- FOLLOWER --}}

    <div class="grid grid-cols-12 mb-4">
      <div class="col-span-3 flex flex-col self-center text-right pr-4">
        <x-label for="follower" />
      </div>
      <div class="col-span-3">
        <x-checkbox name="follower" wire:model="follower" />
      </div>

      {{-- FOLLOWING --}}

      <div class="col-span-3 flex flex-col self-center text-right pr-4">
        <x-label for="following" />
      </div>
      <div class="col-span-3">
        <x-checkbox name="following" wire:model="following" />
      </div>
    </div>

    {{-- RATING --}}

    <div class="grid grid-cols-12 mb-4">
      <div class="col-span-3 flex flex-col self-center text-right pr-4">
        <x-label for="lead_rating" />
      </div>
      <div class="col-span-1">
        <span>A</span>
        <x-input type="radio" name="lead_rating" value="1" wire:model="lead_rating" />
      </div>
      <div class="col-span-1">
        <span>B</span>
        <x-input type="radio" name="lead_rating" value="2" wire:model="lead_rating" />
      </div>
      <div class="col-span-1">
        <span>C</span>
        <x-input type="radio" name="lead_rating" value="3" wire:model="lead_rating" />
      </div>

      {{-- LEAD SOURCE --}}

      <div class="col-span-3 flex flex-col self-center text-right pr-4">
        <x-label for="lead_source" />
      </div>
      <div class="col-span-3">
        <x-checkbox name="lead_source" wire:model="lead_source" />
      </div>
    </div>

    {{-- ALWAYS FOLLOWING --}}

    <div class="grid grid-cols-12 mb-4">
      <div class="col-span-3 flex flex-col self-center text-right pr-4">
        <x-label for="always_following" />
      </div>
      <div class="col-span-3">
        <x-checkbox name="always_following" wire:model="always_following" />
      </div>

      {{-- NEVER FOLLOWING --}}

      <div class="col-span-3 flex flex-col self-center text-right pr-4">
        <x-label for="never_following" />
      </div>
      <div class="col-span-3">
        <x-checkbox name="never_following" wire:model="never_following" />
      </div>
    </div>

    {{-- FAVORITE --}}

    <div class="grid grid-cols-12 mb-10">
      <div class="col-span-3 flex flex-col self-center text-right pr-4">
        <x-label for="favorite" />
      </div>
      <div class="col-span-3">
        <x-checkbox name="favorite" wire:model="favorite" />
      </div>

      {{-- ACTIVE LIKER --}}

      <div class="col-span-3 flex flex-col self-center text-right pr-4">
        <x-label for="active_liker" />
      </div>
      <div class="col-span-3">
        <x-checkbox name="active_liker" wire:model="active_liker" />
      </div>
    </div>

    {{-- SUBMIT BUTTON --}}

    <div class="flex justify-center">
      <button type="submit" wire:click="saveInstagramUser"
        class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 px-8 mx-auto cursor-pointer">
        {{ $buttonLabel }}
      </button>
    </div>

  </section>

</div>
