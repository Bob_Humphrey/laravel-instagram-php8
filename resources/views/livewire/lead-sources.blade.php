  <div class="w-full justify-center py-6">
    <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4">
      Lead Sources
    </h2>

    <table class="w-full">
      @foreach ($instagramUsers as $instagramUser)
        @if ($loop->index % 14 === 0)
          <tr class="grid grid-cols-12 text-sm font-nunito_bold bg-blue-50 border-b border-gray-300">
            <th class="col-span-2 text-left py-3 pl-3">User</th>
            <th class="col-span-3 text-left py-3">User Name</th>
            <th class="col-span-1 text-center py-3">Follower</th>
            <th class="col-span-1 text-center py-3">Following</th>
            <th class="col-span-1 text-center py-3">Ever Follower</th>
            <th class="col-span-1 text-center py-3">Ever Following</th>
            <th class="col-span-1 text-center py-3">Source</th>
            <th class="col-span-1 text-center py-3">Used</th>
            <th class="col-span-1 text-center py-3">Date</th>
          </tr>
        @endif
        <tr
          class="grid grid-cols-12 text-sm font-nunito_light border-b border-gray-300 {{ $loop->odd ? 'bg-gray-50' : '' }}">
          <td class="col-span-2 text-left py-3 pl-3">
            {{ $instagramUser->name }}
          </td>
          <td class="col-span-3 text-left py-3">
            <a href="https://www.instagram.com/{{ $instagramUser->user_name }}/" class="text-blue-500 cursor-pointer"
              target="_blank" rel="noopener noreferrer">
              {{ $instagramUser->user_name }}
            </a>
          </td>
          <td class="col-span-1 flex items-center w-full py-3">
            <div
              class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $instagramUser->follower ? 'bg-green-500' : '' }}">
            </div>
          </td>
          <td class="col-span-1 flex items-center w-full py-3">
            <div
              class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $instagramUser->following ? 'bg-green-500' : '' }}">
            </div>
          </td>
          <td class="col-span-1 flex items-center w-full py-3">
            <div
              class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $instagramUser->ever_follower ? 'bg-green-500' : '' }}">
            </div>
          </td>
          <td class="col-span-1 flex items-center w-full py-3">
            <div
              class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $instagramUser->ever_following ? 'bg-green-500' : '' }}">
            </div>
          </td>
          <td class="col-span-1 flex items-center w-full py-3">
            <div wire:click="toggleLeadSource({{ $instagramUser->id }})"
              class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $instagramUser->lead_source ? 'bg-green-500' : 'bg-gray-300' }}">
            </div>
          </td>
          <td class="col-span-1 flex items-center w-full py-3">
            <div wire:click="toggleLeadSourceUsed({{ $instagramUser->id }})"
              class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $instagramUser->lead_source_used ? 'bg-green-500' : 'bg-gray-300' }}">
            </div>
          </td>
          <td class="col-span-1 text-left py-3 pl-3">
            {{ substr($instagramUser->created_at, 0, 10) }}
          </td>
        </tr>
      @endforeach
    </table>
  </div>
