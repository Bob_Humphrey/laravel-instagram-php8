@php
use Carbon\Carbon;
@endphp

<div class="w-full justify-center py-6">
  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4">
    Followers
  </h2>
  <div class="grid grid-cols-4 mb-4">
    <div class="text-xl font-nunito_light">
      Followers
    </div>
    <div class="text-xl font-nunito_bold">
      {{ $this->state['followers_count'] }}
    </div>
    <div class="text-xl font-nunito_light">
      Start to Follow Back
    </div>
    <div class="text-xl font-nunito_bold">
      {{ $this->state['add_following_count'] }}
    </div>
  </div>
  <table class="w-full">
    @foreach ($this->state['followers'] as $follower)
      @if ($loop->index % 14 === 0)
        <tr class="grid grid-cols-12 text-sm font-nunito_bold bg-blue-50 border-b border-gray-300">
          <th class="col-span-3 flex justify-start py-3 pl-3 cursor-pointer" wire:click="handleSort('name')">
            Name
            <div
              class="{{ $state['sort_field'] === 'name' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'name' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
          <th class="col-span-2 flex justify-start py-3 cursor-pointer" wire:click="handleSort('user_name')">
            User Name
            <div
              class="{{ $state['sort_field'] === 'user_name' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'user_name' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
          <th class="col-span-1 flex justify-center py-3 cursor-pointer" wire:click="handleSort('add_following')">
            Add
            <div
              class="{{ $state['sort_field'] === 'add_following' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'add_following' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
          <th class="col-span-1 flex justify-center py-3 cursor-pointer" wire:click="handleSort('never_following')">
            Never
            <div
              class="{{ $state['sort_field'] === 'never_following' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'never_following' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
          <th class="col-span-1 flex justify-center py-3 cursor-pointer" wire:click="handleSort('favorite')">
            Favorite
            <div
              class="{{ $state['sort_field'] === 'favorite' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'favorite' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
          <th class="col-span-1 flex justify-center py-3 cursor-pointer" wire:click="handleSort('active_liker')">
            Likes
            <div
              class="{{ $state['sort_field'] === 'active_liker' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'active_liker' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
          <th class="col-span-1 flex justify-center py-3 cursor-pointer" wire:click="handleSort('lead_source')">
            Leads
            <div
              class="{{ $state['sort_field'] === 'lead_source' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'lead_source' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
          <th class="col-span-1 flex justify-center py-3 cursor-pointer" wire:click="handleSort('lead_source_used')">
            Used
            <div
              class="{{ $state['sort_field'] === 'lead_source_used' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'lead_source_used' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
          <th class="col-span-1 flex justify-end py-3 pr-3 cursor-pointer"
            wire:click="handleSort('started_follower_date')">
            Start
            <div
              class="{{ $state['sort_field'] === 'started_follower_date' && $state['sort_direction'] === 'ASC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-up />
            </div>
            <div
              class="{{ $state['sort_field'] === 'started_follower_date' && $state['sort_direction'] === 'DESC' ? '' : 'hidden' }} w-5 h-5 text-lg ml-2 overflow-visible">
              <x-zondicon-cheveron-down />
            </div>
          </th>
        </tr>
      @endif
      <tr
        class="grid grid-cols-12 text-sm font-nunito_light border-b border-gray-300 {{ $loop->odd ? '' : 'bg-gray-50' }}">
        <td class="col-span-3 text-left py-3 pl-3">{{ $follower->name }}</td>
        <td class="col-span-2 text-left py-3">
          <a href="https://www.instagram.com/{{ $follower->user_name }}/" class="text-blue-500 cursor-pointer"
            target="_blank" rel="noopener noreferrer">
            {{ $follower->user_name }}
          </a>
        </td>
        <td class="col-span-1 flex items-center w-full py-3">
          <div class="w-5 h-5 rounded-full mx-auto {{ $follower->add_following ? 'bg-green-500' : '' }}">
          </div>
        </td>
        <td class="col-span-1 flex items-center w-full py-3">
          <div
            class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $follower->never_following ? 'bg-red-500' : 'bg-gray-300' }}"
            wire:click="toggleNeverFollow({{ $follower->id }})">
          </div>
        </td>
        <td class="col-span-1 flex items-center w-full py-3">
          <div
            class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $follower->favorite ? 'bg-green-500' : 'bg-gray-300' }}"
            wire:click="toggleFavorites({{ $follower->id }})">
          </div>
        </td>
        <td class="col-span-1 flex items-center w-full py-3">
          <div
            class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $follower->active_liker ? 'bg-green-500' : 'bg-gray-300' }}"
            wire:click="toggleLikes({{ $follower->id }})">
          </div>
        </td>
        <td class="col-span-1 flex items-center w-full py-3">
          <div
            class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $follower->lead_source ? 'bg-green-500' : 'bg-gray-300' }}"
            wire:click="toggleLeadSource({{ $follower->id }})">
          </div>
        </td>
        <td class="col-span-1 flex items-center w-full py-3">
          <div
            class="w-5 h-5 rounded-full cursor-pointer mx-auto {{ $follower->lead_source_used ? 'bg-green-500' : 'bg-gray-300' }}"
            wire:click="toggleLeadSourceUsed({{ $follower->id }})">
          </div>
        </td>
        <td class="col-span-1 text-right py-3 pr-3">
          {{ Carbon::createFromDate($follower->started_follower_date)->format('m-d-y') }}
        </td>
      </tr>
    @endforeach
  </table>
</div>
