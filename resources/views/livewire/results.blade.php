  <div class="w-full justify-center py-6 mx-auto">
    <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4">
      Recent Results
    </h2>

    <table class="w-full">
      @foreach ($results as $result)

        @if ($loop->index % 14 === 0)
          <tr class="grid grid-cols-7 bg-blue-50 text-sm font-nunito_bold border-b border-gray-300">
            <th class="col-span-1 text-left py-3 pl-3">Day</th>
            <th class="col-span-1 text-right py-3 pr-3">Following</th>
            <th class="col-span-1 text-right py-3 pr-3">Follow Back</th>
            <th class="col-span-1 text-right py-3 pr-3">Percent</th>
            <th class="col-span-1 text-right py-3 pr-3">Leads</th>
            <th class="col-span-1 text-right py-3 pr-3">Leads Follow Back</th>
            <th class="col-span-1 text-right py-3 pr-3">Leads Percent</th>
          </tr>
        @endif

        <tr
          class="grid grid-cols-7 text-sm font-nunito_light border-b border-gray-300 {{ $loop->odd ? '' : 'bg-gray-50' }}">
          <td class="col-span-1 text-left py-3 pl-3">
            {{ $result['date'] }}
          </td>
          <td class="col-span-1 text-right py-3 pr-3">
            {{ $result['following'] }}
          </td>
          <td class="col-span-1 text-right py-3 pr-3">
            {{ $result['followers'] }}
          </td>
          <td class="col-span-1 text-right py-3 pr-3">
            {{ $result['percent'] }}%
          </td>
          <td class="col-span-1 text-right py-3 pr-3">
            {{ $result['followingFromLeads'] }}
          </td>
          <td class="col-span-1 text-right py-3 pr-3">
            {{ $result['followersFromLeads'] }}
          </td>
          <td class="col-span-1 text-right py-3 pr-3">
            {{ $result['leadsPercent'] }}%
          </td>
        </tr>
      @endforeach
    </table>
  </div>
